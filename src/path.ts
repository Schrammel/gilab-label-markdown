const actions: Array<Actions> = [ ]
export interface Actions {
  match: ( location: Location ) => boolean
  run: ( window: Window ) => void
}
