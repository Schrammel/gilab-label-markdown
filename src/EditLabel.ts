import { Actions } from './path'
import { ProjectService, LabelsService, WikiService } from './logic/GitService';
import { Project, Wiki, Label } from './gitLab/value';

export default class EditLabels implements Actions {
    static urlRegExp: RegExp =  /[\w\d-]+\/[\w\d-]+\/labels\/([\w\d-]+)/gim
    private _id: number = 0;

    public get id(): number {
        this._id = this._id || getId(window.location, EditLabels.urlRegExp)
        return this._id;
    }

    private div: HTMLDivElement;
    private labelElement: HTMLLabelElement;
    private divTextArea: HTMLDivElement;
    private textarea: HTMLTextAreaElement;
    
    constructor(
        private wikiService: WikiService,
        private project: Project ) {
        if ( !this.match ( window.location ) ) {
            throw 'EditLabels -> Não bate com a url'
        }

        this.wikiService = wikiService
        this.project = project;

        const service = new ProjectService();

        this.textarea = document.createElement('textarea')
        this.textarea.classList.add('form-control');
        this.textarea.disabled = true;
        this.textarea.value = 'carregando';
        this.textarea.style.height = '255px';

        this.divTextArea = document.createElement('div')
        this.divTextArea.classList.add('col-sm-10')
        this.divTextArea.appendChild(this.textarea)

        this.labelElement = document.createElement('label')
        this.labelElement.classList.add('control-label')
        this.labelElement.innerText = 'Markdown auxiliar'

        this.div = document.createElement('div')
        this.div.classList.add('form-group');
        this.div.appendChild(this.labelElement)
        this.div.appendChild(this.divTextArea)
    }

    public static async factory( params: {
        project?: Project,
        wikiService?: WikiService
    } = {} ): Promise<Actions> {
        params = params || {}
        const id = getId(window.location,EditLabels.urlRegExp );
        const project = params.project || await new ProjectService().getProject(location);
        const wikiService = params.wikiService || new WikiService(project);
        return new EditLabels(wikiService, project)
    }

    match(location: Location) {
        let match = EditLabels.urlRegExp.exec(location.pathname.toString());
        
        if (!match) {
            match = EditLabels.urlRegExp.exec(location.pathname.toString());
            if ( !match ) {
                return false;
            }
        }
        if (match.index === EditLabels.urlRegExp.lastIndex) {
            EditLabels.urlRegExp.lastIndex++;
        }
        this._id = +match[1];
       
        return !!this._id;
    }

    async run(window: Window) {
        const location = window.location;
        const id = this.id;
        
        let form = document.getElementById('edit_label') || document.createElement('div');
        let lastChild = form.getElementsByClassName('form-actions')[0]
        form.insertBefore(this.div, lastChild)

        this.textarea.value = 'Carregando dados do projeto'
        this.textarea.value = 'Carregando dados da label'

        const label = await new LabelsService(this.project).get( this.id );
        this.textarea.value = label.markdown || '';
        this.textarea.disabled = false;

        this.textarea.onchange = (event => {
            this.save(label, this.textarea.value);
        })
    }

    async save(label: Label, markdown: string) {
        let wiki: Wiki = {
            content: markdown,
            format: 'markdown',
            slug: label.name,
            title: `label: ${label.name}`
        }
        try {
            await this.wikiService.putByLabel(label, wiki);
        } catch (e) {           
            await this.wikiService.postByLabel( wiki );
        }
    }
}
function getId(location: Location, urlRegExp: RegExp): number {
    let match = urlRegExp.exec(location.pathname.toString());
    // Não sei como, mas não funciona na primeira vez =D.
    if (!match) {
        return 0;
    }
    if (match.index === urlRegExp.lastIndex) {
        urlRegExp.lastIndex++;
    }
    return +match[1];
}
