
export type Label = Label.RootObject;
export type Issue = Issue.RootObject;
export type Wiki = Wiki.RootObject;
export type Project = Project.RootObject;
export type MergeRequest = MergeRequest.RootObject;

export type ProjectQueryObject = Partial<{
    archived: boolean
    visibility: string
    order_by: string
    sort: string
    search: string
    simple: boolean
    owned: boolean
    membership: boolean
    starred: boolean
    statistics: boolean
    with_custom_attributes: boolean
    with_issues_enabled: boolean
    with_merge_requests_enabled: boolean
}>

export type LabelQueryObject = {
    id: number | string
} & Partial<{
    name: string
    per_page: number
    page: number
    color: string
    description: string
    priority: number
}>

export type WikiQueryObject = {
    id: number | string // yes The ID or URL-encoded path of the project
    with_content?: boolean // no Include pages' content
}

export type WikiQueryPK = {
    id: number | string
    slug: string
}

declare namespace Label {

    export interface RootObject {
        id: number;
        name: string;
        color: string;
        description: string;
        open_issues_count: number;
        closed_issues_count: number;
        open_merge_requests_count: number;
        priority?: any;
        subscribed: boolean;
    }

}


declare namespace Issue {

    export interface Author {
        id: number;
        name: string;
        username: string;
        state: string;
        avatar_url: string;
        web_url: string;
    }

    export interface TimeStats {
        time_estimate: number;
        total_time_spent: number;
        human_time_estimate?: any;
        human_total_time_spent?: any;
    }

    export interface RootObject {
        id: number;
        iid: number;
        project_id: number;
        title: string;
        description: string;
        state: string;
        created_at: Date;
        updated_at: Date;
        closed_at?: any;
        closed_by?: any;
        labels: string[];
        milestone?: any;
        assignees: any[];
        author: Author;
        assignee?: any;
        user_notes_count: number;
        upvotes: number;
        downvotes: number;
        due_date?: any;
        confidential: boolean;
        discussion_locked?: any;
        web_url: string;
        time_stats: TimeStats;
        weight?: any;
    }

}

declare namespace Project {

    export interface Links {
        self: string;
        issues: string;
        merge_requests: string;
        repo_branches: string;
        labels: string;
        events: string;
        members: string;
    }

    export interface Owner {
        id: number;
        name: string;
        username: string;
        state: string;
        avatar_url: string;
        web_url: string;
    }

    export interface Namespace {
        id: number;
        name: string;
        path: string;
        kind: string;
        full_path: string;
        parent_id?: any;
    }

    export interface ProjectAccess {
        access_level: number;
        notification_level: number;
    }

    export interface Permissions {
        project_access: ProjectAccess;
        group_access?: any;
    }

    export interface RootObject {
        id: number;
        description: string;
        name: string;
        name_with_namespace: string;
        path: string;
        path_with_namespace: string;
        created_at: Date;
        default_branch?: any;
        tag_list: any[];
        ssh_url_to_repo: string;
        http_url_to_repo: string;
        web_url: string;
        avatar_url?: any;
        star_count: number;
        forks_count: number;
        last_activity_at: Date;
        _links: Links;
        archived: boolean;
        visibility: string;
        owner: Owner;
        resolve_outdated_diff_discussions: boolean;
        container_registry_enabled: boolean;
        issues_enabled: boolean;
        merge_requests_enabled: boolean;
        wiki_enabled: boolean;
        jobs_enabled: boolean;
        snippets_enabled: boolean;
        shared_runners_enabled: boolean;
        lfs_enabled: boolean;
        creator_id: number;
        namespace: Namespace;
        import_status: string;
        open_issues_count: number;
        public_jobs: boolean;
        ci_config_path?: any;
        shared_with_groups: any[];
        only_allow_merge_if_pipeline_succeeds: boolean;
        request_access_enabled: boolean;
        only_allow_merge_if_all_discussions_are_resolved: boolean;
        printing_merge_request_link_enabled: boolean;
        merge_method: string;
        permissions: Permissions;
        approvals_before_merge: number;
    }

}

declare namespace Wiki {

    export interface RootObject {
        content: string;
        format: 'markdown' | 'rdoc' | 'asciidoc';
        slug: string;
        title: string;
    }

}

declare namespace MergeRequest {

    export interface Author {
        state: string;
        web_url: string;
        avatar_url?: any;
        username: string;
        id: number;
        name: string;
    }

    export interface Assignee {
        state: string;
        web_url: string;
        avatar_url?: any;
        username: string;
        id: number;
        name: string;
    }

    export interface Milestone {
        id: number;
        iid: number;
        project_id: number;
        title: string;
        description: string;
        state: string;
        created_at: Date;
        updated_at: Date;
        due_date?: any;
    }

    export interface TimeStats {
        time_estimate: number;
        total_time_spent: number;
        human_time_estimate?: any;
        human_total_time_spent?: any;
    }

    export interface Pipeline {
        id: number;
        ref: string;
        sha: string;
        status: string;
    }

    export interface ClosedBy {
        state: string;
        web_url: string;
        avatar_url?: any;
        username: string;
        id: number;
        name: string;
    }

    export interface RootObject {
        id: number;
        iid: number;
        target_branch: string;
        source_branch: string;
        project_id: number;
        title: string;
        state: string;
        created_at: Date;
        updated_at: Date;
        upvotes: number;
        downvotes: number;
        author: Author;
        assignee: Assignee;
        source_project_id: number;
        target_project_id: number;
        labels: any[];
        description: string;
        work_in_progress: boolean;
        milestone: Milestone;
        merge_when_pipeline_succeeds: boolean;
        merge_status: string;
        subscribed: boolean;
        sha: string;
        merge_commit_sha: string;
        user_notes_count: number;
        changes_count: string;
        should_remove_source_branch: boolean;
        force_remove_source_branch: boolean;
        squash: boolean;
        web_url: string;
        discussion_locked: boolean;
        time_stats: TimeStats;
        approvals_before_merge?: any;
        closed_at: Date;
        latest_build_started_at?: any;
        latest_build_finished_at?: any;
        first_deployed_to_production_at?: any;
        pipeline: Pipeline;
        merged_by?: any;
        merged_at?: any;
        closed_by: ClosedBy;
    }

}

