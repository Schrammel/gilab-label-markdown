import {
    Project,
    Issue,
    Label,
    Wiki,
    ProjectQueryObject,
    LabelQueryObject,
    WikiQueryObject,
    WikiQueryPK,
    MergeRequest
} from './value'

export class GitService {
    private _origin: URL
    private _token: string
    
    constructor(origin: URL, token: string = '') {
        this._token = token
        this._origin = origin
    }
    get origin(): URL {
        return this._origin
    }
    
    public set token( token: string ) {
        this._token = token
    }
    public get token() {
        return this._token
    }
    public fetch(path: URL, init?: GitHeaders): Promise<any> {
        init = init || {}
        init.headers = this.mergeHeaders(init.headers)
        init.credentials = 'include'
        return window.fetch(path.toString(), init).then(response => {
            if (!response.ok) {
                throw `erro no request ${path}`
            }
            return response.json().then(obj => {
                return obj
            })
        })
    }
    public toQuery(url: URL, props: Partial<any>): URL {
        for (const key in props) {
            if (!props.hasOwnProperty(key)) {
                continue
            }
            const value = getProperty(props, key)
            if (value) {
                url.searchParams.append(key, value.toString())
            }
        }
        return url
    }
    public toQueryString(props: Partial<any>): string {
        const params = new URLSearchParams()
        for (const key in props) {
            if (!props.hasOwnProperty(key)) {
                continue
            }
            const value = getProperty(props, key)
            if (value) {
                params.append(key, value)
            }
        }
        return params.toString()
    }
    get projects() {
        return new ProjectService(this)
    }
    get labels() {
        return new LabelsService(this)
    }
    get wikis() {
        return new WikiService(this)
    }
    get issues() {
        return new IssueService(this)
    }
    get merges() {
        return new MergeService(this)
    }
    private mergeHeaders(header: Headers = new Headers()) {
        let newHeaders = new Headers()
        if (this.token) {
            newHeaders.append('PRIVATE-TOKEN', this.token)
        }
        const entrys = header.entries()
        for (let pair of entrys) {
            newHeaders.append(pair[0], pair[1])
        }
        return newHeaders
    }
}
export class ProjectService {
    private url = '/api/v4/projects/'
    constructor(private service: GitService) { }

    public list(query: ProjectQueryObject = {}): Promise<Array<Project>> {
        let url = new URL(this.url, this.service.origin)
        url = this.service.toQuery(url, query)
        return this.service.fetch(url, { cache: 'force-cache' })
    }
}

export class LabelsService {
    constructor(private service: GitService) { }
    public list(query: LabelQueryObject): Promise<Array<Label>> {
        let url = new URL(`/api/v4/projects/${query.id}/labels/`, this.service.origin)
        url = this.service.toQuery(url, query)
        return this.service.fetch(url, { cache: 'force-cache' })
    }
}

export class WikiService {
    constructor(private service: GitService) { }
    public list(query: WikiQueryObject): Promise<Array<Wiki>> {
        let url = new URL(`/api/v4/projects/${query.id}/wikis/`, this.service.origin)
        url = this.service.toQuery(url, query)
        return this.service.fetch(url, { cache: 'force-cache' })
    }
    public get(pk: WikiQueryPK): Promise<Wiki> {
        pk.slug = pk.slug.replace(' ', '-')
        const url = new URL(`/api/v4/projects/${pk.id}/wikis/${pk.slug}`, this.service.origin)
        return this.service.fetch(url, { cache: 'force-cache' })
    }
    public post(id: number, wiki: Wiki): Promise<Wiki> {
        const method = 'POST'
        const url = new URL(`/api/v4/projects/${id}/wikis/`, this.service.origin)
        const body = this.service.toQueryString(wiki)
        const headers = new Headers()
        headers.append('Content-Type', 'application/x-www-form-urlencoded')
        return this.service.fetch(url, { body, headers, method })
    }
    public put(pk: WikiQueryPK, wiki: Wiki): Promise<Wiki> {
        pk.slug = pk.slug.replace(' ', '-')
        let url = new URL(`/api/v4/projects/${pk.id}/wikis/${pk.slug}`, this.service.origin)
        const body = this.service.toQueryString(wiki)
        const headers = new Headers()
        const method = 'PUT'
        headers.append('Content-Type', 'application/x-www-form-urlencoded')
        return this.service.fetch(url, { body, headers, method })
    }
}

export class IssueService {
    constructor(private service: GitService) { }

    put(id: number, project: Project, props: Partial<Issue>, location: Location = window.location): Promise<Issue> {
        let url = new URL(`/api/v4/projects/${project.id}/issues/${id}`, location.origin);
        url = this.service.toQuery(url, props)
        return this.service.fetch(url, { method: 'PUT', body: JSON.stringify(props) });
    }

    post(id: number, project: Project, props: Issue, location: Location = window.location): Promise<Issue> {
        const url = new URL(`/api/v4/projects/${project.id}/issues/${id}`, location.origin);
        return this.service.fetch(url, { method: 'PUT', body: JSON.stringify(props) });
    }

    get(id: number, project: Project, location: Location = window.location): Promise<Issue> {
        const url = new URL(`/api/v4/projects/${project.id}/issues/${id}`, location.origin);
        return this.service.fetch(url);
    }

}

export class MergeService {
    constructor(private service: GitService) { }

    put(
        mergeRequestIid: number, 
        project: Project, 
        props: Partial<MergeRequest>, 
        location: Location = window.location): Promise<MergeRequest> {
        let url = new URL(`/api/v4/projects/${project.id}/merge_requests/${mergeRequestIid}`, location.origin);
        url = this.service.toQuery(url, props)
        return this.service.fetch(url, { method: 'PUT', body: JSON.stringify(props) });
    }

    post(
        mergeRequestIid: number, 
        project: Project, 
        props: MergeRequest,
        location: Location = window.location): Promise<MergeRequest> {
        const url = new URL(`/api/v4/projects/${project.id}/merge_requests/${mergeRequestIid}`, location.origin);
        return this.service.fetch(url, { method: 'PUT', body: JSON.stringify(props) });
    }

    get(
        mergeRequestIid: number, 
        project: Project, 
        location: Location = window.location): Promise<MergeRequest> {
        const url = new URL(`/api/v4/projects/${project.id}/merge_requests/${mergeRequestIid}`, location.origin);
        return this.service.fetch(url);
    }

}

interface GitHeaders {
    body?: FormData | string | null
    cache?: RequestCache
    credentials?: RequestCredentials
    headers?: Headers
    integrity?: string
    keepalive?: boolean
    method?: string
    mode?: RequestMode
    redirect?: RequestRedirect
    referrer?: string
    referrerPolicy?: ReferrerPolicy
    signal?: AbortSignal
    window?: any
}

function getProperty<T, K extends keyof T>(o: T, name: K): T[K] {
    return o[name]
}

export default new GitService( new URL(window.location.origin) );
