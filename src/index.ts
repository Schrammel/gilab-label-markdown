import EditLabel from './EditLabel';
import IssueLabel from './EditIssue';
import EditMarge from './EditMerge'

import 'babel-polyfill';
import { Actions } from './path';

(async () => {
    
    const actions: Actions[] = [];
    try {
        actions.push( await EditLabel.factory() )
    } catch ( e ) {
        console.log(e)
    }
    try {
        actions.push( await EditMarge.factory() )
    } catch ( e ) {
        console.log(e)
    }
    try {
        actions.push( await IssueLabel.factory() )
    } catch ( e ) {
        console.log(e)
    }
    
    actions.filter(action => action.match(location))
        .forEach(action => {
            action.run(window)
        })
})()
