import { Actions } from './path'
import { ProjectService, LabelsService, IssueService, NewLabel } from './logic/GitService';
import { Project, Issue, Label } from './gitLab/value';
import LabelFactory from './logic/LabelFactory'

export default class IssueLabel implements Actions {
    
    static urlRegExp = /[\w\d-]+\/[\w\d-]+\/issues\/([\d-]+)/gmi;
    labelFactory: LabelFactory;
    loading: HTMLAnchorElement;
    grid: HTMLDivElement;
    div: HTMLDivElement;
    row: HTMLDivElement;
    labels: HTMLDivElement;
    
    private _id: number = 0;

    public get id(): number {
        this._id = this._id || getId(window.location, IssueLabel.urlRegExp)
        return this._id;
    }

    constructor(
        private project: Project
    ) {
        if ( !this.match ( window.location ) ) {
            throw 'IssueLabel -> Não bate com a url'
        }
        const projectService = new ProjectService();
        
        this.div = document.createElement('div');
        this.div.classList.add('content-block')
        this.div.classList.add('emoji-block')
        
        this.row = document.createElement('div');
        this.row.classList.add('row')
        
        this.grid = document.createElement('div')
        this.grid.classList.add('col-sm-12')
        
        this.labels = document.createElement('div');
        this.labels.classList.add('dont-hide')
        this.labels.classList.add('has-labels')
        this.labels.classList.add('hide-collapsed')
        this.labels.classList.add('issuable-show-labels')
        this.labels.classList.add('value')
        const varival = 's';
        
        this.div.appendChild(this.row)
        this.row.appendChild(this.grid)
        this.grid.appendChild(this.labels)

        const span = document.createElement('span');
        span.classList.add('label')
        span.classList.add('color-label')
        span.classList.add('label')
        span.classList.add('has-tooltip')
        span.textContent = 'Carregando Labels....'
        span.style.color = '#2e2e2e'
        this.loading = document.createElement('a');
        this.loading.href = '#';
        this.loading.appendChild(span);
        this.labels.appendChild(this.loading);
        
        
        this.labelFactory = new LabelFactory( this.id, new IssueService(this.project) )
    }
    public static async factory( params: {
        project?: Project
    } = {} ): Promise<Actions> {
        return new IssueLabel(
            params.project || await new ProjectService().getProject(location)
        )
    }
    match (location: Location) {
        let enable = !!location.pathname.match( IssueLabel.urlRegExp )
        return enable;
    }

    async run(window: Window) {
        const location = window.location;
        const id = getId(location, IssueLabel.urlRegExp);

        const div = document.getElementsByClassName('emoji-block')[0];
        const parent = div.parentElement;

        if (parent) {
            parent.insertBefore(this.div, div)
        }

        const labels = await new LabelsService(this.project).getAll(location)
        this.loading.remove();
        labels.filter(label => label.markdown)
            .forEach((label) => {
                    const item = this.labelFactory.getSpan(label);
                    this.labels.appendChild(item)
            })
    }
}

function getId(location: Location = window.location, urlRegExp: RegExp): number {
    let match = urlRegExp.exec(location.pathname.toString());
    
    // Não sei como, mas não funciona na primeira vez =D.
    if (!match) {
        return getId(location, urlRegExp);
    }
    if (match.index === urlRegExp.lastIndex) {
        urlRegExp.lastIndex++;
    }
    return +match[1];
}
