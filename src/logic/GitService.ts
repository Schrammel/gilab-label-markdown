import { Project, Issue, Wiki, Label, MergeRequest } from '../gitLab/value'
import GitService from '../gitLab/GitService';

export type NewLabel = Label & {
    markdown?: string
    url?: string
};
export type NewWiki = Wiki & {
    url?: string
};

const gitService = GitService;
let key = window.location.origin

chrome.storage.local.get([key], function( result ) {
    let token = result[key];
    if ( !token ) {
        token = prompt('Favor insira o token');
        if ( token ) {
            let obj: any = {}
            obj[key] = token
            chrome.storage.local.set(obj);
        }
    }
    gitService.token = token;
});

export class ProjectService {
    private url = new URL('/api/v4/projects/', location.origin);
    async getProject(location: Location): Promise<Project> {
        const projectName = location.pathname.split('/')[2];
        const projects = await gitService.projects.list({
            membership: true,
            search: projectName
        })
        const project = projects.find(project => !!~location.pathname.indexOf(project.path_with_namespace))
        if (project) {
            return project
        }
        throw 'nao encontrado o projeto'
    }
}

export class LabelsService {
    private project: Project;
    constructor(project: Project) {
        this.project = project;
    }

    async getAll(location: Location = window.location): Promise<Array<NewLabel>> {
        const service = gitService;
        const projectService = new ProjectService();
        let page = 0;
        let perPage = 99;
        let labels = [];
        let temp = [];
        do {
            page++;
            temp = await service.labels.list({ id: this.project.id, per_page: perPage, page })
            labels.push( ...temp.map(label => label as NewLabel) )
        } while (temp.length );
        return await Promise.all(labels.map(label => {
            return this.mergeLabelWiki(label)
        }));
    }

    async get(id: number, location: Location = window.location): Promise<NewLabel> {
        let page = 0;
        let perPage = 99;
        const service = gitService;
        const projectService = new ProjectService();

        let label: NewLabel | undefined;
        let labels: Array<NewLabel> = [];
        do {
            page++;
            labels = await service.labels.list({ id: this.project.id, per_page: perPage, page })
            label = labels.map(label => label as NewLabel).find(label => label.id === id)
        } while (!label || !labels.length );

        if (label) {
            return this.mergeLabelWiki(label)
        }
        throw 'erro ao obetr a label'
    }

    private async mergeLabelWiki(label: NewLabel) {
        const wikiservice = new WikiService(this.project)
        try {
            const wiki = await wikiservice.findByLabel(label)
            label.markdown = wiki.content;
            label.url = `[${wiki.title}](${wiki.url})`
        } catch (e) { 
            console.error(e)
        }
        return label;
    }
}

export class WikiService {
    private storage: Storage;
    private project: Project;
    constructor(project: Project) {
        this.project = project;
        this.storage = window.localStorage;
    }

    async getAll(location: Location = window.location): Promise<Array<NewWiki>> {
        const git = gitService
        const wikis = await git.wikis.list(this.project)
        return wikis.map( wiki => {
            return this.toNewWiki(wiki) }
        );
    }

    async findByLabel(label: Label, location: Location = window.location): Promise<NewWiki> {
        const git = gitService
        const wiki = await git.wikis.get({
            id: this.project.id,
            slug: `label: ${label.name}`
        })
        return this.toNewWiki(wiki);
    }

    async putByLabel(label: Label, wiki: Wiki, location: Location = window.location): Promise<NewWiki> {
        const git = gitService
        const headers = new Headers()
        const slug = `label: ${label.name}`;
        return git.wikis.put({
            id: this.project.id,
            slug: `label: ${label.name}`
        }, wiki)
    }

    async postByLabel(wiki: Wiki, location = window.location): Promise<NewWiki> {
        const git = gitService
        const headers = new Headers()
        return git.wikis.post(this.project.id, wiki)
    }

    private toNewWiki( wiki: Wiki, location: Location = window.location): NewWiki {
        const newWiki: NewWiki = wiki;
        newWiki.content = wiki.content + '\n\n'
        newWiki.url = `${location.origin}/${this.project.path_with_namespace}/wikis/${wiki.slug}`
        return wiki
    }
}

export class IssueService {
    private project: Project;
    constructor(project: Project) {
        this.project = project;
    }
    put(id: number, props: Partial<Issue>, location: Location = window.location): Promise<Issue> {
       return gitService.issues.put( id, this.project, props, location );
    }

    post(id: number, props: Issue, location: Location = window.location): Promise<Issue> {
        return gitService.issues.post( id, this.project, props, location );
    }

    get(id: number, location: Location = window.location): Promise<Issue> {
        return gitService.issues.get( id, this.project, location );
    }
}

export class MergeRequestService {
    private project: Project;
    constructor(project: Project) {
        this.project = project;
    }
    put(id: number, props: Partial<MergeRequest>, location: Location = window.location): Promise<MergeRequest> {
       return gitService.merges.put( id, this.project, props, location );
    }

    post(id: number, props: MergeRequest, location: Location = window.location): Promise<MergeRequest> {
        return gitService.merges.post( id, this.project, props, location );
    }

    get(id: number, location: Location = window.location): Promise<MergeRequest> {
        return gitService.merges.get( id, this.project, location );
    }
}
