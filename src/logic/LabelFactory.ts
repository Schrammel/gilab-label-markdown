import { NewLabel } from '../logic/GitService';

export default class LabelFactory {

    constructor(
        private id: number,
        private service: {put: ( iid: number, value: { description: string }) => Promise<any>
        get: ( iid: number ) => Promise<any> } 
    ) {
     
    }

    appendDescrition(element: HTMLElement, label: NewLabel) {
        this.service.get(this.id).then(issue => {
            let description = issue.description;
            description += `\n${label.markdown}\n\n\n\n${label.url}\n\n`;
             this.service.put( issue.iid, { description } ).then( () => {
                element.remove();
            })
        })
    }

    getSpan( label: NewLabel ) {
        const span = document.createElement('span');
        span.classList.add('label')
        span.classList.add('color-label')
        span.classList.add('label')
        span.classList.add('has-tooltip')
        span.style.backgroundColor = label.color;
        span.style.color = '#FFFFFF';
        span.textContent = label.name

        const link = document.createElement('a');
        link.href = '#';
        link.onclick = () => {
            this.appendDescrition(span, label)
        }
        link.appendChild(span);
        return link;
    }
}
