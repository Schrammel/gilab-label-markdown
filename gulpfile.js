const gulp = require('gulp')
const manifestObject = require('./manifest.js')
const fs = require('fs')
const pack = require('./package.json')

function manifest () {
  manifestObject.version = pack.version
  fs.writeFile('dist/manifest.json', JSON.stringify(manifestObject), 'utf8', () => console.log('arquivo exportado'))
}

function moveFiles () {
  gulp.src([
    './options.js',
    './options.html',
    './**.png'
  ]).pipe(gulp.dest('dist/'))
}

gulp.task('manifest', manifest)
gulp.task('moveFiles', moveFiles)
gulp.task('default', ['moveFiles'])